if not WarBoard_TogglerSquared then WarBoard_TogglerSquared = {} end
local WarBoard_TogglerSquared = WarBoard_TogglerSquared
local modName = "WarBoard_TogglerSquared"
local modLabel = "Squared"
local LabetSetTextColor = LabelSetTextColor
local Tooltips = Tooltips

function WarBoard_TogglerSquared.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "squaredicon", 0, 0) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 78, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonDown", "WarBoard_TogglerSquared.ToggleActive")
		LibWBToggler.RegisterEvent(modName, "OnRButtonDown", "Squared.ShowSettings")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerSquared.ShowStatus")
		if Squared.GetSetting("disable") then LabelSetTextColor(modName.."Label", 255, 0, 0) else LabelSetTextColor(modName.."Label", 0, 255, 0) end
	end
end

function WarBoard_TogglerSquared.ToggleActive()
	Squared.ToggleActive()
	if Squared.GetSetting("disable") then LabelSetTextColor(modName.."Label", 255, 0, 0) else LabelSetTextColor(modName.."Label", 0, 255, 0) end
	WarBoard_TogglerSquared.ShowStatus()
end

function WarBoard_TogglerSquared.ShowStatus()
	Tooltips.CreateTextOnlyTooltip(modName, nil)
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
	Tooltips.SetTooltipText(1, 1, L"Squared Toggler")
	if Squared.GetSetting("disable") then Tooltips.SetTooltipColor(1, 1, 255, 0, 0) else Tooltips.SetTooltipColor(1, 1, 0, 255, 0) end
	Tooltips.SetTooltipText(2, 1, L"Left Click:")
	Tooltips.SetTooltipText(2, 3, L"Toggle Active")
	Tooltips.SetTooltipText(3, 1, L"Right Click:")
	Tooltips.SetTooltipText(3, 3, L"Toggle GUI")
	Tooltips.Finalize()
end
